## detecting the opening of door or slide of wardrobe for lighting to triger other actions
## written by Han Yu, 2021

import spidev
import time
import numpy as np
 
spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=100
 
def readChannel(channel):
  val = spi.xfer2([1,(8+channel)<<4,0])
  data = ((val[1]&3) << 8) + val[2]
  return data

t=1
for t in range (500):
  Step_length=2
  Total_time=t*Step_length 
  time.sleep(Step_length)
  
  if __name__ == "__main__":
   v=(readChannel(0)/1023.0)*3.3
   dist = 16.2537 * v**4 - 129.893 * v**3 + 382.268 * v**2 - 512.611 * v + 301.439
   pc=(dist/80)*100
  if pc > 90:
     state='OpenStatus3' # status of door or slide is fully open, can check if the lighting is on
  elif pc > 60:
     state='OpenStatus2' # door or slide is partly open, can send the signal to other smart switch for lighting e.g.
  elif pc > 30:
     state='OpenStatus1' # door or slide is about to open
  else:
     state='Closed' # door or slide is closed, turn off the light for the wardrobe

  print (state)
  t+=1
