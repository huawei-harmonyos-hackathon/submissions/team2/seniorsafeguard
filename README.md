# HarmonyHomeSafeguard

Project space for git versioning of smart home assistant for anomaly detection such as sudden fall, smoke & fire, break in etc as well as people's sleeping quality, health conditions, based on Raspberry Pi, AI, Omron thermal sensor (C file) & range sensor (Python).


## Name
Stay safe at home: a simple Pi-sensor system

## Description

The project of "Stay Safe At Home" is to supervise people's activiy, safety and health condition by simple raspberry Pi system with various Sensors. For instance, with the information from thermal sensor and sound sensor, your daily activity, sleeping equality, noise and safety at home can be recorded and analyzed. The report can help you to improve your life quality, the alert can be sent to your neighours, friends and event your doctor when emergency occurs. 
In this intial stage, we focus more on methodology, novelty and the research of market potential as well as the first launch of sensor system with raspberry Pi. 
The launch method of Omron thermal sensor can be found in their webpage in Github below:
https://github.com/omron-devhub/d6t-2jcieev01-raspberrypi

We also uploaded a modified version in this readme file based on "d6t-44l.c" file in this repository to further recognize the human activity. But due to the difficulty to official hardware combo as originally planned, the code can't be validated. So the uploaded code is only recommended for preliminary test. A stable code version will be validated & finalized upon the delivery of offical sensor package.

To test the remote control and analysis of data from sensor system, we use an IR range sensor system with Raspberry Pi as example, the code "range-sensor.py" to conduct the remote control is also uploaded with Python language. The purpose is to show case possibility using range sensor to detect general door opening or wardrobe sliding status to trigger other home applications e.g. lighting. The range sensor can be replaced with Ormon thermal sensors for more profouding detection of movement pattern, than specific detection of certain movement on version 1.

## Installation
The installation includes: 

1. Launch the raspberry Pi: https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/0
2. Launch a wireless network to remotely control the raspberry Pi system, you can choose "PuTTY" platform to do this. This will link the Pi with other devices which share the same Wi-fi network.
3. Integrate the sensor system into Pi, in our project, the integration and launching of Omron thermal sensor can be seen in the Ormon's webpage under Github: https://github.com/omron-devhub/d6t-2jcieev01-raspberrypi. The C language is used.
4. Reading, recording and analyzing the sensor data. We are still putting efforts in this part.
   

## Support
For further questions pls send email to han.yu@tvrl.lth.se

## Roadmap
Further integration with Ormon sensor combo is planned, version 2 with test output data will be released upon Dec, 2021.

## Authors and acknowledgment
Dr. Han Yu, Dr. Dauren Mussabek and Sida Jiang for development of version 1 coding for basic set up of back-up Ormon thermal 4*4 sensor wired with Raspberry Pi. 

## License
 * MIT License
 * Copyright (c) 2019, 2018 - present OMRON Corporation
 * All rights reserved.


## Project status
Minimum prototype version 1 with basic sensor set-up. 

